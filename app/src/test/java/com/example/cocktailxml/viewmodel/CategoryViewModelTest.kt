package com.example.cocktailxml.viewmodel

import com.example.cocktailxml.model.CocktailRepo
import com.example.cocktailxml.model.entity.Category
import com.example.cocktailxml.util.CoroutinesTestExtension
import com.example.cocktailxml.util.InstantTaskExecutorExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(InstantTaskExecutorExtension::class)
@OptIn(ExperimentalCoroutinesApi::class)
@ExtendWith(CoroutinesTestExtension::class)
internal class CategoryViewModelTest {
    private val repo = mockk<CocktailRepo>()

    @Test
    @DisplayName("Tests retrieving the category list")
    fun testGetCategories() = runTest {
        // given
        val expected = listOf(Category("lol"))
        coEvery { repo.getCategories() } coAnswers { expected }

        // when
        val vm = CategoryViewModel(repo)

        // then
        Assertions.assertFalse(vm.state.value?.isLoading ?: true)
        Assertions.assertEquals(expected, vm.state.value!!.categories)
    }
}
