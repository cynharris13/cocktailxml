package com.example.cocktailxml.model

import com.example.cocktailxml.model.dto.category.CategoryDTO
import com.example.cocktailxml.model.dto.category.CategoryResponse
import com.example.cocktailxml.model.dto.categorydrink.CategoryDrinkDTO
import com.example.cocktailxml.model.dto.categorydrink.CategoryDrinkResponse
import com.example.cocktailxml.model.dto.drink.DrinkDTO
import com.example.cocktailxml.model.dto.drink.DrinkResponse
import com.example.cocktailxml.model.entity.Category
import com.example.cocktailxml.model.entity.CategoryDrink
import com.example.cocktailxml.model.entity.Drink
import com.example.cocktailxml.model.remote.CocktailService
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@OptIn(ExperimentalCoroutinesApi::class)
internal class CocktailRepoTest {
    private val service = mockk<CocktailService>()
    private val repo = CocktailRepo(service)

    @Test
    @DisplayName("Tests getting the category list")
    fun testGetCategories() = runTest {
        // given
        val dtos = CategoryResponse(listOf(CategoryDTO("lol")))
        coEvery { service.getCategories().isSuccessful } coAnswers { true }
        coEvery { service.getCategories().body() } coAnswers { dtos }
        val expected = listOf(Category("lol"))

        // when
        val actual = repo.getCategories()

        // then
        Assertions.assertEquals(expected, actual)
    }

    @Test
    @DisplayName("Tests getting the category drink list from a category")
    fun testGetCategoryDrinks() = runTest {
        // given
        val dtos = CategoryDrinkResponse(listOf(CategoryDrinkDTO("", "lol", "")))
        coEvery { service.getCategoryDrinks("lol").isSuccessful } coAnswers { true }
        coEvery { service.getCategoryDrinks("lol").body() } coAnswers { dtos }
        val expected = listOf(CategoryDrink("lol", ""))

        // when
        val actual = repo.getCategoryDrinks("lol")

        // then
        Assertions.assertEquals(expected, actual)
    }

    @Test
    @DisplayName("Tests getting the drink list from a drink name")
    fun testGetDrinks() = runTest {
        // given
        val dtos = DrinkResponse(listOf(DrinkDTO(strDrink = "lol")))
        coEvery { service.getDrinks("lol").isSuccessful } coAnswers { true }
        coEvery { service.getDrinks("lol").body() } coAnswers { dtos }
        val expected = listOf(Drink("", "lol", null, "", "", "", "", null, emptyList(), emptyList()))

        // when
        val actual = repo.getDrinks("lol")

        // then
        Assertions.assertEquals(expected, actual)
    }
}
