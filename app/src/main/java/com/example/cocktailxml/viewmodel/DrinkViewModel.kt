package com.example.cocktailxml.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktailxml.model.CocktailRepo
import com.example.cocktailxml.view.drink.DrinkState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Drink view model.
 *
 * @constructor Create empty Drink view model
 */
@HiltViewModel
class DrinkViewModel @Inject constructor(val repo: CocktailRepo) : ViewModel() {
    private val _state = MutableLiveData(DrinkState())
    val state: LiveData<DrinkState> get() = _state

    /**
     * Get drinks from the [CocktailRepo].
     *
     * @param drinkName the type of drink to fetch.
     */
    fun getDrinks(drinkName: String) = viewModelScope.launch {
        _state.value = _state.value?.copy(isLoading = true)
        val drinks = repo.getDrinks(drinkName)
        _state.value = _state.value?.copy(isLoading = false, drinks = drinks)
    }
}
