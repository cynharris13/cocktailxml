package com.example.cocktailxml.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktailxml.model.CocktailRepo
import com.example.cocktailxml.view.category.CategoryState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Category view model.
 *
 * @property repo used to make requests for data to display in the view.
 * @constructor Create empty Category view model
 */
@HiltViewModel
class CategoryViewModel @Inject constructor(private val repo: CocktailRepo) : ViewModel() {
    private val _state = MutableLiveData(CategoryState())
    val state: LiveData<CategoryState> get() = _state

    init {
        getCategories()
    }

    private fun getCategories() = viewModelScope.launch {
        _state.value = _state.value?.copy(isLoading = true)
        val categories = repo.getCategories().sortedBy { it.name }
        _state.value = _state.value?.copy(isLoading = false, categories = categories)
    }
}
