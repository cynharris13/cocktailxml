package com.example.cocktailxml.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktailxml.model.CocktailRepo
import com.example.cocktailxml.view.categorydrink.CategoryDrinkState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Category drink view model.
 *
 * @property repo to fetch the cateogry drinks.
 * @constructor Create empty Category drink view model
 */
@HiltViewModel
class CategoryDrinkViewModel @Inject constructor(private val repo: CocktailRepo) : ViewModel() {
    private val _state = MutableLiveData(CategoryDrinkState())
    val state: LiveData<CategoryDrinkState> get() = _state

    /**
     * Get category drinks.
     *
     * @param categoryName the category to get drinks from
     */
    fun getCategoryDrinks(categoryName: String) = viewModelScope.launch {
        _state.value = _state.value?.copy(isLoading = true)
        val categoryDrinks = repo.getCategoryDrinks(categoryName)
        _state.value = _state.value?.copy(isLoading = false, categoryDrinks = categoryDrinks)
    }
}
