package com.example.cocktailxml.model.mapper

/**
 * Mapper interface.
 *
 * @param DTO to be converted
 * @param ENTITY to be created
 * @constructor Create empty Mapper
 */
interface Mapper<in DTO, out ENTITY> {
    /**
     * Invoke.
     *
     * @param dto
     * @return
     */
    operator fun invoke(dto: DTO): ENTITY
}
