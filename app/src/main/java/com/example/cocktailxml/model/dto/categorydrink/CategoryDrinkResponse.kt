package com.example.cocktailxml.model.dto.categorydrink

@kotlinx.serialization.Serializable
data class CategoryDrinkResponse(
    val drinks: List<CategoryDrinkDTO> = emptyList()
)
