package com.example.cocktailxml.model.remote

import com.example.cocktailxml.model.dto.category.CategoryResponse
import com.example.cocktailxml.model.dto.categorydrink.CategoryDrinkResponse
import com.example.cocktailxml.model.dto.drink.DrinkResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Cocktail service to retrieve the data with retrofit.
 *
 * @constructor Create empty Cocktail service
 */
interface CocktailService {
    @GET(CATEGORY_ENDPOINT)
    suspend fun getCategories(): Response<CategoryResponse>

    @GET(CATEGORY_DRINK_ENDPOINT)
    suspend fun getCategoryDrinks(
        @Query(CATEGORY_DRINK_FETCHED) categoryName: String
    ): Response<CategoryDrinkResponse>

    @GET(DRINK_ENDPOINT)
    suspend fun getDrinks(@Query(DRINK_FETCHED) drinkName: String): Response<DrinkResponse>

    companion object {
        private const val DRINK_FETCHED = "s"
        private const val CATEGORY_DRINK_FETCHED = "c"
        private const val CATEGORY_ENDPOINT = "list.php?c=list"
        private const val CATEGORY_DRINK_ENDPOINT = "filter.php"
        private const val DRINK_ENDPOINT = "search.php"
    }
}
