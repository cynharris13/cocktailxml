package com.example.cocktailxml.model.entity

/**
 * Category drink object.
 *
 * @property drinkName
 * @property drinkThumbnail
 * @constructor Create empty Category drink
 */
data class CategoryDrink(
    val drinkName: String,
    val drinkThumbnail: String
)
