package com.example.cocktailxml.model.dto.drink

@kotlinx.serialization.Serializable
data class DrinkResponse(
    val drinks: List<DrinkDTO> = emptyList()
)
