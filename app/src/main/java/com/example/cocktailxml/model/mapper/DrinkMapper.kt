package com.example.cocktailxml.model.mapper

import com.example.cocktailxml.model.dto.drink.DrinkDTO
import com.example.cocktailxml.model.entity.Drink

/**
 * Drink mapper to convert [DrinkDTO] to [Drink].
 *
 * @constructor Create empty Drink mapper
 */
class DrinkMapper : Mapper<DrinkDTO, Drink> {
    override fun invoke(dto: DrinkDTO): Drink = dto.let {
        return Drink(
            strAlcoholic = it.strAlcoholic ?: "",
            strDrink = it.strDrink ?: "",
            strDrinkAlternate = it.strDrinkAlternate,
            strDrinkThumb = it.strDrinkThumb ?: "",
            strGlass = it.strGlass ?: "",
            strImageSource = it.strImageSource ?: "",
            strInstructions = it.strInstructions ?: "",
            strVideo = it.strVideo,
            strIngredients = listOf(
                it.strIngredient1 ?: "",
                it.strIngredient2 ?: "",
                it.strIngredient3 ?: "",
                it.strIngredient4 ?: "",
                it.strIngredient5 ?: "",
                it.strIngredient6 ?: "",
                it.strIngredient7 ?: "",
                it.strIngredient8 ?: "",
                it.strIngredient9 ?: "",
                it.strIngredient10 ?: "",
                it.strIngredient11 ?: "",
                it.strIngredient12 ?: "",
                it.strIngredient13 ?: "",
                it.strIngredient14 ?: "",
                it.strIngredient15 ?: ""
            ).filter { ingredient -> ingredient.isNotEmpty() },
            strMeasures = listOf(
                it.strMeasure1 ?: "",
                it.strMeasure2 ?: "",
                it.strMeasure3 ?: "",
                it.strMeasure4 ?: "",
                it.strMeasure5 ?: "",
                it.strMeasure6 ?: "",
                it.strMeasure7 ?: "",
                it.strMeasure8 ?: "",
                it.strMeasure9 ?: "",
                it.strMeasure10 ?: "",
                it.strMeasure11 ?: "",
                it.strMeasure12 ?: "",
                it.strMeasure13 ?: "",
                it.strMeasure14 ?: "",
                it.strMeasure15 ?: ""
            ).filter { measure -> measure.isNotEmpty() }
        )
    }
}
