package com.example.cocktailxml.model.dto.categorydrink

@kotlinx.serialization.Serializable
data class CategoryDrinkDTO(
    val idDrink: String,
    val strDrink: String,
    val strDrinkThumb: String
)
