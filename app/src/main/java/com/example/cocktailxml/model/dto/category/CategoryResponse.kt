package com.example.cocktailxml.model.dto.category

@kotlinx.serialization.Serializable
data class CategoryResponse(
    val drinks: List<CategoryDTO> = emptyList()
)
