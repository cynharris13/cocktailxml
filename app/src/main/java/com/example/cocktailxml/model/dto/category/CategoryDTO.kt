package com.example.cocktailxml.model.dto.category

@kotlinx.serialization.Serializable
data class CategoryDTO(
    val strCategory: String
)
