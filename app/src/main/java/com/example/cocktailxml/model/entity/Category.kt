package com.example.cocktailxml.model.entity

/**
 * Drink data class.
 *
 * @property name to display
 * @constructor Create empty Drink
 */
data class Category(
    val name: String
)
