package com.example.cocktailxml.model

import com.example.cocktailxml.model.dto.category.CategoryResponse
import com.example.cocktailxml.model.dto.categorydrink.CategoryDrinkResponse
import com.example.cocktailxml.model.dto.drink.DrinkResponse
import com.example.cocktailxml.model.entity.Category
import com.example.cocktailxml.model.entity.CategoryDrink
import com.example.cocktailxml.model.entity.Drink
import com.example.cocktailxml.model.mapper.DrinkMapper
import com.example.cocktailxml.model.remote.CocktailService
import javax.inject.Inject

/**
 * Cocktail repository to make requests of [CocktailService].
 *
 * @property cocktailService the service to request from the API.
 * @constructor Create empty Cocktail repo
 */
class CocktailRepo @Inject constructor(private val cocktailService: CocktailService) {
    val drinkMapper = DrinkMapper()

    /**
     * Get categories from the service.
     *
     * @return a [List] of [Category]
     */
    suspend fun getCategories(): List<Category> {
        val categoryResponse = cocktailService.getCategories()
        return if (categoryResponse.isSuccessful) {
            val response = categoryResponse.body() ?: CategoryResponse()
            response.drinks.map { Category(name = it.strCategory) }
        } else {
            emptyList()
        }
    }

    /**
     * Get category drinks.
     *
     * @param categoryName the category of which to return drinks
     * @return a [List] of [CategoryDrink]
     */
    suspend fun getCategoryDrinks(categoryName: String): List<CategoryDrink> {
        val catDrinkResponse = cocktailService.getCategoryDrinks(categoryName)
        return if (catDrinkResponse.isSuccessful) {
            val response = catDrinkResponse.body() ?: CategoryDrinkResponse()
            response.drinks.map { CategoryDrink(drinkName = it.strDrink, drinkThumbnail = it.strDrinkThumb) }
        } else { emptyList() }
    }

    /**
     * Get drinks.
     *
     * @param drinkName the drink of choice.
     * @return at least one way to make said drink.
     */
    suspend fun getDrinks(drinkName: String): List<Drink> {
        val drinkResponse = cocktailService.getDrinks(drinkName)
        return if (drinkResponse.isSuccessful) {
            val response = drinkResponse.body() ?: DrinkResponse()
            response.drinks.map { drinkMapper(it) }
        } else { emptyList() }
    }
}
