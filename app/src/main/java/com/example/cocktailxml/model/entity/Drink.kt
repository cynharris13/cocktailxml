package com.example.cocktailxml.model.entity

/**
 * Drink object.
 *
 * @property strAlcoholic
 * @property strDrink
 * @property strDrinkAlternate
 * @property strDrinkThumb
 * @property strGlass
 * @property strImageSource
 * @property strInstructions
 * @property strVideo
 * @property strIngredients
 * @property strMeasures
 * @constructor Create empty Drink
 */
data class Drink(
    val strAlcoholic: String,
    val strDrink: String,
    val strDrinkAlternate: String?,
    val strDrinkThumb: String,
    val strGlass: String,
    val strImageSource: String,
    val strInstructions: String,
    val strVideo: String?,
    val strIngredients: List<String>,
    val strMeasures: List<String>
)
