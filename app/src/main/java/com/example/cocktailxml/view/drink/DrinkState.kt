package com.example.cocktailxml.view.drink

import com.example.cocktailxml.model.entity.Drink

/**
 * Drink state.
 *
 * @property isLoading true while fetching drinks
 * @property drinks the resulting list of drinks
 * @constructor Create empty Drink state
 */
data class DrinkState(
    val isLoading: Boolean = false,
    val drinks: List<Drink> = emptyList()
)
