package com.example.cocktailxml.view.categorydrink

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.cocktailxml.databinding.SingleItemBinding
import com.example.cocktailxml.model.entity.CategoryDrink

/**
 * Rv adapter to process the recycle view.
 *
 * @constructor Create empty Rv adapter
 */
class CategoryDrinkAdapter(
    private val onDrinkClicked: (CategoryDrink) -> Unit
) : RecyclerView.Adapter<CategoryDrinkAdapter.ViewHolder>() {
    private val categoryDrinkList: MutableList<CategoryDrink> = mutableListOf()

    /** create an inner class with name ViewHolder.
     * It takes a view argument, in which pass the generated class of single_item.xml
     * ie SingleItemBinding and in the RecyclerView.ViewHolder(binding.root) pass it like this
     */
    inner class ViewHolder(private val binding: SingleItemBinding) : RecyclerView.ViewHolder(binding.root) {
        /**
         * Binds a [CategoryDrink] name to the [SingleItemBinding].
         *
         * @param categoryDrink the category drink to be bound
         */
        fun bindCategoryDrink(categoryDrink: CategoryDrink) = with(binding) {
            this.category.text = categoryDrink.drinkName
            this.imageView.load(categoryDrink.drinkThumbnail)
        }
    }

    // inside the onCreateViewHolder inflate the view of SingleItemBinding
    // and return new ViewHolder object containing this layout
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = SingleItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding).apply {
            itemView.setOnClickListener {
                onDrinkClicked(categoryDrinkList[adapterPosition])
            }
        }
    }

    // bind the items with each item
    // of the list languageList
    // which than will be
    // shown in recycler view
    // to keep it simple we are
    // not setting any image data to view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindCategoryDrink(categoryDrinkList[position])
    }

    // return the size of matchList
    override fun getItemCount(): Int = categoryDrinkList.size

    /**
     * Add drinks to the list.
     *
     * @param categoryDrinks
     */
    fun addCategoryDrinks(categoryDrinks: List<CategoryDrink>) {
        val startPosition = categoryDrinkList.size
        categoryDrinkList.addAll(categoryDrinks)
        notifyItemRangeInserted(startPosition, categoryDrinks.size)
    }
}
