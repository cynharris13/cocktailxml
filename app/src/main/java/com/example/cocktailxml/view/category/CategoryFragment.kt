package com.example.cocktailxml.view.category

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cocktailxml.R
import com.example.cocktailxml.databinding.FragmentFirstBinding
import com.example.cocktailxml.viewmodel.CategoryViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
@AndroidEntryPoint
class CategoryFragment : Fragment() {
    private val categoryViewModel by viewModels<CategoryViewModel>()
    private var _binding: FragmentFirstBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    // create reference to the adapter and the list
    // in the list pass the model of Language
    private val categoryAdapter: CategoryAdapter by lazy {
        CategoryAdapter {
            val args = bundleOf("categoryName" to it.name)
            findNavController().navigate(R.id.action_CategoryFragment_to_categoryDrinkFragment, args)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        // create  layoutManager
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(requireContext())

        // pass it to rvLists layoutManager
        binding.rvList.layoutManager = layoutManager
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        categoryViewModel.state.observe(viewLifecycleOwner) { state ->
            // initialize the adapter,
            // and pass the required argument
            categoryAdapter.addCategories(state.categories)
            // attach adapter to the recycler view
            binding.rvList.adapter = categoryAdapter
        }
        binding.rvList.setOnClickListener {
            findNavController().navigate(R.id.action_CategoryFragment_to_categoryDrinkFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
