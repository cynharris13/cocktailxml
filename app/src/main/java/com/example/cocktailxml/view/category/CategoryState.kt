package com.example.cocktailxml.view.category

import com.example.cocktailxml.model.entity.Category

/**
 * Category state that holds data loaded from the view model.
 *
 * @property isLoading true while the fetch is running
 * @property categories the retrieved data
 * @constructor Create empty Category state
 */
data class CategoryState(
    val isLoading: Boolean = false,
    val categories: List<Category> = emptyList()
)
