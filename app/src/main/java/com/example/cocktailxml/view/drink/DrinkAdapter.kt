package com.example.cocktailxml.view.drink

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.cocktailxml.databinding.SingleDrinkBinding
import com.example.cocktailxml.model.entity.Drink

/**
 * Drink adapter.
 *
 * @constructor Create empty Drink adapter
 */
class DrinkAdapter : RecyclerView.Adapter<DrinkAdapter.ViewHolder>() {
    private val drinkList: MutableList<Drink> = mutableListOf()

    /**
     * View holder.
     *
     * @property binding
     * @constructor Create empty View holder
     */
    inner class ViewHolder(private val binding: SingleDrinkBinding) : RecyclerView.ViewHolder(binding.root) {
        /**
         * Binds a [Drink] to the [SingleDrinkBinding].
         *
         * @param drink the drink to be bound
         */
        fun bindDrink(drink: Drink) = with(binding) {
            var drinkName = drink.strDrink
            val alcoholicText = "${drink.strAlcoholic}, best served in a ${drink.strGlass}"
            var ingredientText = ""
            for (i in 0 until drink.strIngredients.size) {
                ingredientText += "${drink.strMeasures[i]} ${drink.strIngredients[i]}\n"
            }
            if (drink.strDrinkAlternate != null) {
                drinkName += "/${drink.strDrinkAlternate}"
            }
            this.imageViewDrink.load(drink.strImageSource)
            this.drinkName.text = drinkName
            this.instructions.text = drink.strInstructions
            this.alcoholic.text = alcoholicText
            this.ingredientsAndMeasures.text = ingredientText
            this.video.text = drink.strVideo
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = SingleDrinkBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    // bind the items with each item
    // of the list languageList
    // which than will be
    // shown in recycler view
    // to keep it simple we are
    // not setting any image data to view
    override fun onBindViewHolder(holder: DrinkAdapter.ViewHolder, position: Int) {
        holder.bindDrink(drinkList[position])
    }

    // return the size of matchList
    override fun getItemCount(): Int = drinkList.size

    /**
     * Add drinks to the list.
     *
     * @param
     */
    fun addDrinks(drinks: List<Drink>) {
        val startPosition = drinkList.size
        drinkList.addAll(drinks)
        notifyItemRangeInserted(startPosition, drinks.size)
    }
}
