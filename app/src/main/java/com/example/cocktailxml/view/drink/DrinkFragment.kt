package com.example.cocktailxml.view.drink

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cocktailxml.databinding.FragmentDrinkBinding
import com.example.cocktailxml.viewmodel.DrinkViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
@AndroidEntryPoint
class DrinkFragment : Fragment() {
    private val drinkViewModel by viewModels<DrinkViewModel>()
    private var _binding: FragmentDrinkBinding? = null

    private val drinkName by lazy { arguments?.getString("drinkName") ?: "" }

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    // create reference to the adapter and the list
    // in the list pass the model of Language
    private val rvAdapter: DrinkAdapter by lazy { DrinkAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDrinkBinding.inflate(inflater, container, false)
        // create  layoutManager
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(requireContext())

        drinkViewModel.getDrinks(drinkName)

        // pass it to rvLists layoutManager
        binding.rvList.layoutManager = layoutManager
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        drinkViewModel.state.observe(viewLifecycleOwner) { state ->
            // initialize the adapter,
            // and pass the required argument
            rvAdapter.addDrinks(state.drinks)
            // attach adapter to the recycler view
            binding.rvList.adapter = rvAdapter
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
