package com.example.cocktailxml.view.categorydrink

import com.example.cocktailxml.model.entity.CategoryDrink

/**
 * Category drink state.
 *
 * @property isLoading true while the view model is fetching
 * @property categoryDrinks the resulting list of [CategoryDrink]
 * @constructor Create empty Category drink state
 */
data class CategoryDrinkState(
    val isLoading: Boolean = false,
    val categoryDrinks: List<CategoryDrink> = emptyList()
)
