package com.example.cocktailxml.view.category

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.cocktailxml.databinding.SingleItemBinding
import com.example.cocktailxml.model.entity.Category

/**
 * Rv adapter to process the recycle view.
 *
 * @constructor Create empty Rv adapter
 */
class CategoryAdapter(
    private val categoryClicked: (Category) -> Unit
) : RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {
    private val categoryList: MutableList<Category> = mutableListOf()

    /** create an inner class with name ViewHolder.
     * It takes a view argument, in which pass the generated class of single_item.xml
     * ie SingleItemBinding and in the RecyclerView.ViewHolder(binding.root) pass it like this
     */
    inner class ViewHolder(private val binding: SingleItemBinding) : RecyclerView.ViewHolder(binding.root) {
        /**
         * Binds a [Category] league name to the [SingleItemBinding].
         *
         * @param category the match to be bound
         */
        fun bindCategory(category: Category) = with(binding) {
            this.category.text = category.name
        }
    }

    // inside the onCreateViewHolder inflate the view of SingleItemBinding
    // and return new ViewHolder object containing this layout
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = SingleItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding).apply {
            itemView.setOnClickListener {
                val cat = categoryList[adapterPosition]
                categoryClicked(cat)
            }
        }
    }

    // bind the items with each item
    // of the list languageList
    // which than will be
    // shown in recycler view
    // to keep it simple we are
    // not setting any image data to view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindCategory(categoryList[position])
    }

    // return the size of matchList
    override fun getItemCount(): Int = categoryList.size

    /**
     * Add drinks to the list.
     *
     * @param categories
     */
    fun addCategories(categories: List<Category>) {
        val startPosition = categoryList.size
        categoryList.addAll(categories)
        notifyItemRangeInserted(startPosition, categories.size)
    }
}
