package com.example.cocktailxml.view.categorydrink

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cocktailxml.R
import com.example.cocktailxml.databinding.FragmentCategoryDrinkBinding
import com.example.cocktailxml.viewmodel.CategoryDrinkViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
@AndroidEntryPoint
class CategoryDrinkFragment : Fragment() {
    private val categoryDrinkViewModel by viewModels<CategoryDrinkViewModel>()
    private var _binding: FragmentCategoryDrinkBinding? = null

    private val categoryName by lazy { arguments?.getString("categoryName") ?: "" }

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    // create reference to the adapter and the list
    // in the list pass the model of Language
    private val rvAdapter: CategoryDrinkAdapter by lazy {
        CategoryDrinkAdapter {
            val args = bundleOf("drinkName" to it.drinkName)
            findNavController().navigate(R.id.action_categoryDrinkFragment_to_drinkFragment, args)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCategoryDrinkBinding.inflate(inflater, container, false)
        // create  layoutManager
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(requireContext())

        categoryDrinkViewModel.getCategoryDrinks(categoryName)

        // pass it to rvLists layoutManager
        binding.rvList.layoutManager = layoutManager
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        categoryDrinkViewModel.state.observe(viewLifecycleOwner) { state ->
            // initialize the adapter,
            // and pass the required argument
            rvAdapter.addCategoryDrinks(state.categoryDrinks)
            // attach adapter to the recycler view
            binding.rvList.adapter = rvAdapter
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
