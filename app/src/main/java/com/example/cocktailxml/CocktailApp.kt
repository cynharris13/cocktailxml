package com.example.cocktailxml

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Cocktail app override for Hilt.
 *
 * @constructor Create empty Cocktail app
 */
@HiltAndroidApp
class CocktailApp : Application()
