package com.example.meals

import android.app.Application
import android.content.Context
import dagger.hilt.android.HiltAndroidApp

/**
 * Meal application.
 *
 * @constructor Create empty Meal app
 */
@HiltAndroidApp
class MealApp : Application() {
    init {
        instance = this
    }
    companion object {
        private var instance: MealApp? = null

        /**
         * App context.
         *
         * @return
         */
        fun appContext(): Context {
            return instance!!.applicationContext
        }
    }
}
