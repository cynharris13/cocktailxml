package com.example.meals.view.area

import com.example.meals.model.entity.Area
import com.example.meals.model.entity.MealByFilter

/**
 * Area state.
 *
 * @property isLoading true while fetching [areas]
 * @property areas the resulting [List] of [Area]
 * @property selected the [Area] selected by the user
 * @property mealsByArea the list returned by [selected]
 * @constructor Create empty Area state
 */
data class AreaState(
    val isLoading: Boolean = false,
    val areas: List<Area> = emptyList(),
    val selected: Area? = null,
    val mealsByArea: List<MealByFilter> = emptyList()
)
