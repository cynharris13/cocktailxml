package com.example.meals.view.ingredient

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.meals.databinding.SimpleItemBinding
import com.example.meals.model.entity.Ingredient

/**
 * Ingredient adapter to present the [List] of [Ingredient].
 *
 * @constructor Create empty Category adapter
 */
class IngredientAdapter(
    private val onClick: (Ingredient) -> Unit
) : RecyclerView.Adapter<IngredientAdapter.ViewHolder>() {
    private val ingredientList = mutableListOf<Ingredient>()

    /**
     * View holder for [IngredientAdapter].
     *
     * @property binding
     * @constructor Create empty View holder
     */
    inner class ViewHolder(private val binding: SimpleItemBinding) : RecyclerView.ViewHolder(binding.root) {
        /**
         * Bind ingredient to the [RecyclerView].
         *
         * @param i the area to be bound
         */
        fun bindIngredient(i: Ingredient) = with(binding) {
            textField.text = i.ingredientName
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = SimpleItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding).apply {
            itemView.setOnClickListener {
                onClick(ingredientList[adapterPosition])
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindIngredient(ingredientList[position])
    }

    override fun getItemCount(): Int = ingredientList.size

    /**
     * Add ingredients to the [ingredientList].
     *
     * @param ingredients to be added
     */
    fun addIngredients(ingredients: List<Ingredient>) {
        val amount = ingredientList.size
        var amountAdded = 0
        for (ingredient in ingredients) {
            if (!ingredientList.contains(ingredient)) {
                ingredientList.add(ingredient)
                amountAdded++
            }
        }
        notifyItemRangeInserted(amount, amountAdded)
    }
}
