package com.example.meals.view.saveditems

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.meals.databinding.SavedItemBinding
import com.example.meals.model.entity.Favorite

/**
 * Meals by filter adapter.
 *
 * @constructor Create empty Meals by filter adapter
 */
class FavoritesAdapter(
    private val onItemClick: (Favorite) -> Unit,
    private val onRemove: (Favorite) -> Unit
) : RecyclerView.Adapter<FavoritesAdapter.ViewHolder>() {
    private val favoriteList: MutableList<Favorite> = mutableListOf()

    /**
     * View holder.
     *
     * @property binding
     * @constructor Create empty View holder
     */
    inner class ViewHolder(private val binding: SavedItemBinding) : RecyclerView.ViewHolder(binding.root) {
        /**
         * Bind meal to the screen.
         *
         * @param favorite
         */
        fun bindMeal(favorite: Favorite) = with(binding) {
            mealName.text = favorite.mealName
            imageView.load(favorite.mealThumb)
            instructions.text = favorite.instructions
            itemView.setOnClickListener {
                onItemClick(favoriteList[adapterPosition])
            }
            button.setOnClickListener {
                val fav = favoriteList[adapterPosition]
                onRemove(fav)
                removeFavorite(fav)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = SavedItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindMeal(favoriteList[position])
    }

    override fun getItemCount(): Int = favoriteList.size

    /**
     * Add meals to the [favoriteList].
     *
     * @param favorites
     */
    fun addFavorites(favorites: List<Favorite>) {
        val amount = favoriteList.size
        var amountAdded = 0
        for (favorite in favorites) {
            if (!favoriteList.contains(favorite)) {
                favoriteList.add(favorite)
                amountAdded++
            }
        }
        notifyItemRangeInserted(amount, amountAdded)
    }

    /**
     * Remove favorite from the [favoriteList].
     *
     * @param favorite
     */
    fun removeFavorite(favorite: Favorite) {
        val position = favoriteList.indexOf(favorite)
        favoriteList.remove(favorite)
        notifyItemRemoved(position)
    }
}
