package com.example.meals.view.saveditems

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.meals.databinding.FragmentSavedItemsBinding
import com.example.meals.viewmodel.SavedItemsViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Meal by category fragment.
 *
 * @constructor Create empty Meal by category fragment
 */
@AndroidEntryPoint
class SavedItemsFragment : Fragment() {
    private val savedItemsViewModel by viewModels<SavedItemsViewModel>()
    private var _binding: FragmentSavedItemsBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val favoriteAdapter by lazy {
        FavoritesAdapter(
            onItemClick = {
                val directions = SavedItemsFragmentDirections.actionSavedItemsFragmentToMealDetails(it.id.toString())
                findNavController().navigate(directions)
            },
            onRemove = {
                savedItemsViewModel.removeFavorite(it)
            }
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSavedItemsBinding.inflate(inflater, container, false)

        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(requireContext())
        binding.rvList.layoutManager = layoutManager
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        savedItemsViewModel.state.observe(viewLifecycleOwner) {
            favoriteAdapter.addFavorites(it.favorites)
            binding.rvList.adapter = favoriteAdapter
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
