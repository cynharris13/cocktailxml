package com.example.meals.view.mealdetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import coil.load
import com.example.meals.databinding.FragmentMealDetailsBinding
import com.example.meals.model.entity.MealDetails
import com.example.meals.viewmodel.MealDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Meal by category fragment.
 *
 * @constructor Create empty Meal by category fragment
 */
@AndroidEntryPoint
class MealDetailsFragment : Fragment() {
    private val mealViewModel by viewModels<MealDetailsViewModel>()
    private var _binding: FragmentMealDetailsBinding? = null

    private val args by navArgs<MealDetailsFragmentArgs>()

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMealDetailsBinding.inflate(inflater, container, false)
        mealViewModel.getMealDetails(args.mealID)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mealViewModel.state.observe(viewLifecycleOwner) { state ->
            if (state.meal != null) {
                with(binding) {
                    mealName.text = state.meal.mealName
                    imageView.load(state.meal.mealThumb)
                    instructions.text = state.meal.instructions
                    var ingredientText = ""
                    for (ingredient in state.meal.ingredientsToMeasures) {
                        ingredientText += "${ingredient.value} ${ingredient.key}\n"
                    }
                    ingredients.text = ingredientText
                    if (state.isFavorite) {
                        addFavorite(state.meal)
                    } else { removeFavorite(state.meal) }
                }
            }
        }
    }

    private fun addFavorite(mealDetails: MealDetails) {
        val addText = "Add favorite"
        binding.button.text = addText
        binding.button.setOnClickListener {
            mealViewModel.addFavorite(mealDetails)
            removeFavorite(mealDetails)
        }
    }

    private fun removeFavorite(mealDetails: MealDetails) {
        val removeText = "Remove favorite"
        binding.button.text = removeText
        binding.button.setOnClickListener {
            mealViewModel.removeFavorite(mealDetails)
            addFavorite(mealDetails)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
