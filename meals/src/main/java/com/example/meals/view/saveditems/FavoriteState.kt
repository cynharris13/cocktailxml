package com.example.meals.view.saveditems

import com.example.meals.model.entity.Favorite

/**
 * Favorite state.
 *
 * @property isLoading
 * @property favorites
 * @constructor Create empty Favorite state
 */
data class FavoriteState(
    val isLoading: Boolean = false,
    val favorites: List<Favorite> = emptyList()
)
