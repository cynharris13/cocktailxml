package com.example.meals.view.mealsbyfilter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.meals.databinding.FragmentMealByAreaBinding
import com.example.meals.viewmodel.IngredientViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Meals by ingredient fragment.
 *
 * @constructor Create empty Meals by ingredient fragment
 */
@AndroidEntryPoint
class MealsByIngredientFragment : Fragment() {
    private val ingredientViewModel by activityViewModels<IngredientViewModel>()
    private var _binding: FragmentMealByAreaBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val mealsByIngredientAdapter by lazy {
        MealsByFilterAdapter {
            val directions = MealsByIngredientFragmentDirections.actionMealsByIngredientFragmentToMealDetails(it.mealId)
            findNavController().navigate(directions)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMealByAreaBinding.inflate(inflater, container, false)

        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(requireContext())
        binding.rvList.layoutManager = layoutManager
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ingredientViewModel.state.observe(viewLifecycleOwner) {
            mealsByIngredientAdapter.addMeals(it.mealsByIngredient)
            binding.rvList.adapter = mealsByIngredientAdapter
            binding.areaName.text = it.selected?.ingredientName ?: "No Ingredient Selected"
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
