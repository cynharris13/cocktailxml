package com.example.meals.view.ingredient

import com.example.meals.model.entity.Ingredient
import com.example.meals.model.entity.MealByFilter

/**
 * Ingredient state.
 *
 * @property isLoading true while fetching
 * @property ingredients the resulting list
 * @property selected the ingredient selected by the user
 * @property mealsByIngredient the list of meals given [selected]
 * @constructor Create empty Ingredient state
 */
data class IngredientState(
    val isLoading: Boolean = false,
    val ingredients: List<Ingredient> = emptyList(),
    val selected: Ingredient? = null,
    val mealsByIngredient: List<MealByFilter> = emptyList()
)
