package com.example.meals.view.mealsbyfilter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.meals.databinding.MealItemBinding
import com.example.meals.model.entity.MealByFilter

/**
 * Meals by filter adapter.
 *
 * @constructor Create empty Meals by filter adapter
 */
class MealsByFilterAdapter(
    private val onClick: (MealByFilter) -> Unit
) : RecyclerView.Adapter<MealsByFilterAdapter.ViewHolder>() {
    private val mealByFilterList: MutableList<MealByFilter> = mutableListOf()

    /**
     * View holder.
     *
     * @property binding
     * @constructor Create empty View holder
     */
    inner class ViewHolder(private val binding: MealItemBinding) : RecyclerView.ViewHolder(binding.root) {
        /**
         * Bind meal to the screen.
         *
         * @param mealByFilter
         */
        fun bindMeal(mealByFilter: MealByFilter) = with(binding) {
            mealName.text = mealByFilter.mealName
            imageView.load(mealByFilter.mealThumb)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = MealItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding).apply {
            itemView.setOnClickListener {
                onClick(mealByFilterList[adapterPosition])
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindMeal(mealByFilterList[position])
    }

    override fun getItemCount(): Int = mealByFilterList.size

    /**
     * Add meals to the [mealByFilterList].
     *
     * @param meals
     */
    fun addMeals(meals: List<MealByFilter>) {
        val amount = mealByFilterList.size
        var amountAdded = 0
        for (meal in meals) {
            if (!mealByFilterList.contains(meal)) {
                mealByFilterList.add(meal)
                amountAdded++
            }
        }
        notifyItemRangeInserted(amount, amountAdded)
    }
}
