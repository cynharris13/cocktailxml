package com.example.meals.view.mealsbyfilter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.meals.databinding.FragmentMealByAreaBinding
import com.example.meals.viewmodel.AreaViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Meals by area fragment.
 *
 * @constructor Create empty Meals by area fragment
 */
@AndroidEntryPoint
class MealsByAreaFragment : Fragment() {
    private val areaViewModel by activityViewModels<AreaViewModel>()
    private var _binding: FragmentMealByAreaBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val mealsByAreaAdapter by lazy {
        MealsByFilterAdapter {
            val directions = MealsByAreaFragmentDirections.actionMealsByAreaFragmentToMealDetails(it.mealId)
            findNavController().navigate(directions)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMealByAreaBinding.inflate(inflater, container, false)

        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(requireContext())
        binding.rvList.layoutManager = layoutManager
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        areaViewModel.state.observe(viewLifecycleOwner) {
            mealsByAreaAdapter.addMeals(it.mealsByArea)
            binding.rvList.adapter = mealsByAreaAdapter
            binding.areaName.text = it.selected?.areaName ?: "No Area Selected"
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
