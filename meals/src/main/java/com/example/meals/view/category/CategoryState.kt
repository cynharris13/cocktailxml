package com.example.meals.view.category

import com.example.meals.model.entity.Category
import com.example.meals.model.entity.MealByFilter

/**
 * Category state.
 *
 * @property isLoading true while fetching
 * @property categories the resulting list
 * @property selected the category selected by the user
 * @property mealsByCategory the meals returned from [selected]
 * @constructor Create empty Category state
 */
data class CategoryState(
    val isLoading: Boolean = false,
    val categories: List<Category> = emptyList(),
    val selected: Category? = null,
    val mealsByCategory: List<MealByFilter> = emptyList()
)
