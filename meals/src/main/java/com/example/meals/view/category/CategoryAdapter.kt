package com.example.meals.view.category

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.meals.databinding.SingleItemBinding
import com.example.meals.model.entity.Category

/**
 * Category adapter to present the [List] of [Category].
 *
 * @constructor Create empty Category adapter
 */
class CategoryAdapter(private val onClick: (Category) -> Unit) : RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {
    private val categoryList = mutableListOf<Category>()

    /**
     * View holder for [CategoryAdapter].
     *
     * @property binding
     * @constructor Create empty View holder
     */
    inner class ViewHolder(private val binding: SingleItemBinding) : RecyclerView.ViewHolder(binding.root) {
        /**
         * Bind category to the [RecyclerView].
         *
         * @param cat the category to be bound
         */
        fun bindCategory(cat: Category) = with(binding) {
            category.text = cat.categoryName
            imageView.load(cat.categoryThumb)
            description.text = cat.categoryDescription
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = SingleItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding).apply {
            itemView.setOnClickListener {
                onClick(categoryList[adapterPosition])
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindCategory(categoryList[position])
    }

    override fun getItemCount(): Int = categoryList.size

    /**
     * Add categories to the [categoryList].
     *
     * @param categories to be added
     */
    fun addCategories(categories: List<Category>) {
        val amount = categoryList.size
        var amountAdded = 0
        for (category in categories) {
            if (!categoryList.contains(category)) {
                categoryList.add(category)
                amountAdded++
            }
        }
        notifyItemRangeInserted(amount, amountAdded)
    }
}
