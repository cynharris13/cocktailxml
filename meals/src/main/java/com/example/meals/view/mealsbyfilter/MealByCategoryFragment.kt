package com.example.meals.view.mealsbyfilter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.meals.databinding.FragmentCategoryByMealBinding
import com.example.meals.viewmodel.CategoryViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Meal by category fragment.
 *
 * @constructor Create empty Meal by category fragment
 */
@AndroidEntryPoint
class MealByCategoryFragment : Fragment() {
    private val categoryViewModel by activityViewModels<CategoryViewModel>()
    private var _binding: FragmentCategoryByMealBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val mealsByCategoryAdapter by lazy {
        MealsByFilterAdapter {
            val directions = MealByCategoryFragmentDirections.actionMealByCategoryFragmentToMealDetails(it.mealId)
            findNavController().navigate(directions)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCategoryByMealBinding.inflate(inflater, container, false)

        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(requireContext())
        binding.rvList.layoutManager = layoutManager
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        categoryViewModel.state.observe(viewLifecycleOwner) {
            mealsByCategoryAdapter.addMeals(it.mealsByCategory)
            binding.rvList.adapter = mealsByCategoryAdapter
            binding.categoryName.text = it.selected?.categoryName ?: "No Category Selected"
            binding.description.text = it.selected?.categoryDescription ?: "Description here"
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
