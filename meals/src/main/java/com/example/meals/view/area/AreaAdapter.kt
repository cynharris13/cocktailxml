package com.example.meals.view.area

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.meals.databinding.SimpleItemBinding
import com.example.meals.model.entity.Area
import com.example.meals.model.entity.Category

/**
 * Area adapter to present the [List] of [Category].
 *
 * @constructor Create empty Category adapter
 */
class AreaAdapter(private val onClick: (Area) -> Unit) : RecyclerView.Adapter<AreaAdapter.ViewHolder>() {
    private val areaList = mutableListOf<Area>()

    /**
     * View holder for [AreaAdapter].
     *
     * @property binding
     * @constructor Create empty View holder
     */
    inner class ViewHolder(private val binding: SimpleItemBinding) : RecyclerView.ViewHolder(binding.root) {
        /**
         * Bind area to the [RecyclerView].
         *
         * @param a the area to be bound
         */
        fun bindArea(a: Area) = with(binding) {
            textField.text = a.areaName
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = SimpleItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding).apply {
            itemView.setOnClickListener {
                onClick(areaList[adapterPosition])
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindArea(areaList[position])
    }

    override fun getItemCount(): Int = areaList.size

    /**
     * Add areas to the [areaList].
     *
     * @param areas to be added
     */
    fun addAreas(areas: List<Area>) {
        val amount = areaList.size
        var amountAdded = 0
        for (area in areas) {
            if (!areaList.contains(area)) {
                areaList.add(area)
                amountAdded++
            }
        }
        notifyItemRangeInserted(amount, amountAdded)
    }
}
