package com.example.meals.view.mealdetails

import com.example.meals.model.entity.MealDetails

/**
 * Meal state holder.
 *
 * @property isLoading true while fetching
 * @property meal the resulting meal
 * @property isFavorite whether the meal already exists as a favorite
 * @constructor Create empty Meal state
 */
data class MealState(
    val isLoading: Boolean = false,
    val meal: MealDetails? = null,
    val isFavorite: Boolean = false
)
