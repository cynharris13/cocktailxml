package com.example.meals.view.ingredient

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.meals.R
import com.example.meals.databinding.FragmentIngredientBinding
import com.example.meals.viewmodel.IngredientViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
@AndroidEntryPoint
class IngredientFragment : Fragment() {
    private val ingredientViewModel by activityViewModels<IngredientViewModel>()
    private var _binding: FragmentIngredientBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val ingredientAdapter by lazy {
        IngredientAdapter {
            ingredientViewModel.getMealByIngredient(it)
            findNavController().navigate(R.id.action_ingredientFragment_to_mealsByIngredientFragment)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentIngredientBinding.inflate(inflater, container, false)

        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(requireContext())
        binding.rvList.layoutManager = layoutManager

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ingredientViewModel.state.observe(viewLifecycleOwner) {
            ingredientAdapter.addIngredients(it.ingredients)
            binding.rvList.adapter = ingredientAdapter
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
