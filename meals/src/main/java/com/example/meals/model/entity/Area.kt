package com.example.meals.model.entity

/**
 * Area data holder.
 *
 * @property areaName
 * @constructor Create empty Area
 */
data class Area(
    val areaName: String
)
