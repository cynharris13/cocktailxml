package com.example.meals.model

import com.example.meals.model.dto.area.AreaResponse
import com.example.meals.model.dto.category.CategoryResponse
import com.example.meals.model.dto.category.MealByFilterResponse
import com.example.meals.model.dto.ingredients.IngredientResponse
import com.example.meals.model.dto.mealdetails.MealDetailsResponse
import com.example.meals.model.entity.Area
import com.example.meals.model.entity.Category
import com.example.meals.model.entity.Favorite
import com.example.meals.model.entity.Ingredient
import com.example.meals.model.entity.MealByFilter
import com.example.meals.model.entity.MealDetails
import com.example.meals.model.local.FavoriteDatabase
import com.example.meals.model.remote.MealService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * Meal repository.
 *
 * @property mealService to fetch data from the API
 * @property favoriteDatabase the db to save favorite meals
 * @constructor Create empty Meal repo
 */
class MealRepo @Inject constructor(
    private val mealService: MealService,
    private val favoriteDatabase: FavoriteDatabase
) {
    private val favDao = favoriteDatabase.favoriteDao()

    /**
     * Get categories from [MealService].
     *
     * @return the complete [List] of [Category]
     */
    suspend fun getCategories(): List<Category> {
        val response = mealService.getMealCategories()
        return if (response.isSuccessful) {
            val categoryResponse = response.body() ?: CategoryResponse()
            categoryResponse.categories.map {
                Category(it.strCategory, it.strCategoryDescription, it.strCategoryThumb)
            }
        } else { emptyList() }
    }

    /**
     * Get areas.
     *
     * @return the [List] of [Area]
     */
    suspend fun getAreas(): List<Area> {
        val response = mealService.getAreas()
        return if (response.isSuccessful) {
            val areaResponse = response.body() ?: AreaResponse()
            areaResponse.meals.map { Area(it.strArea) }
        } else { emptyList() }
    }

    /**
     * Get ingredients.
     *
     * @return the [List] of [Ingredient]
     */
    suspend fun getIngredients(): List<Ingredient> {
        val response = mealService.getIngredients()
        return if (response.isSuccessful) {
            val ingredientResponse = response.body() ?: IngredientResponse()
            ingredientResponse.meals.map { Ingredient(it.strIngredient) }
        } else { emptyList() }
    }

    /**
     * Get meals by filter.
     *
     * @param filterName to display meals of
     * @param filterType whether to fetch by category, area, or ingredient
     * @return a [List] of [MealByFilter]
     */
    suspend fun getMealsByFilter(filterName: String, filterType: FilterType): List<MealByFilter> {
        val response = when (filterType) {
            FilterType.CATEGORY -> mealService.getMealByCategory(filterName)
            FilterType.AREA -> mealService.getMealByArea(filterName)
            FilterType.INGREDIENT -> mealService.getMealByIngredient(filterName)
        }
        return if (response.isSuccessful) {
            val mealResponse = response.body() ?: MealByFilterResponse()
            mealResponse.meals.map { MealByFilter(it.idMeal, it.strMeal, it.strMealThumb) }
        } else { emptyList() }
    }

    /**
     * Get meal details.
     *
     * @param id
     * @return the [MealDetails] fetched.
     */
    suspend fun getMealDetails(id: String): MealDetails {
        val response = mealService.getMealDetailsById(id)
        return if (response.isSuccessful) {
            val mealResponse = response.body() ?: MealDetailsResponse()
            val meal = mealResponse.meals.first()
            meal.let {
                MealDetails(
                    id = it.idMeal,
                    mealName = it.strMeal ?: "",
                    instructions = it.strInstructions ?: "",
                    mealThumb = it.strMealThumb ?: "",
                    ingredientsToMeasures = mapOf(
                        (it.strIngredient1 ?: "") to (it.strMeasure1 ?: ""),
                        (it.strIngredient2 ?: "") to (it.strMeasure2 ?: ""),
                        (it.strIngredient3 ?: "") to (it.strMeasure3 ?: ""),
                        (it.strIngredient4 ?: "") to (it.strMeasure4 ?: ""),
                        (it.strIngredient5 ?: "") to (it.strMeasure5 ?: ""),
                        (it.strIngredient6 ?: "") to (it.strMeasure6 ?: ""),
                        (it.strIngredient7 ?: "") to (it.strMeasure7 ?: ""),
                        (it.strIngredient8 ?: "") to (it.strMeasure8 ?: ""),
                        (it.strIngredient9 ?: "") to (it.strMeasure9 ?: ""),
                        (it.strIngredient10 ?: "") to (it.strMeasure10 ?: ""),
                        (it.strIngredient11 ?: "") to (it.strMeasure11 ?: ""),
                        (it.strIngredient12 ?: "") to (it.strMeasure12 ?: ""),
                        (it.strIngredient13 ?: "") to (it.strMeasure13 ?: ""),
                        (it.strIngredient12 ?: "") to (it.strMeasure12 ?: ""),
                        (it.strIngredient13 ?: "") to (it.strMeasure13 ?: ""),
                        (it.strIngredient14 ?: "") to (it.strMeasure14 ?: ""),
                        (it.strIngredient15 ?: "") to (it.strMeasure15 ?: ""),
                        (it.strIngredient16 ?: "") to (it.strMeasure16 ?: ""),
                        (it.strIngredient17 ?: "") to (it.strMeasure17 ?: ""),
                        (it.strIngredient18 ?: "") to (it.strMeasure18 ?: ""),
                        (it.strIngredient19 ?: "") to (it.strMeasure19 ?: ""),
                        (it.strIngredient20 ?: "") to (it.strMeasure20 ?: "")
                    ).filter { map -> map.key.isNotEmpty() }
                )
            }
        } else { MealDetails("", "", "", "", emptyMap()) }
    }

    /**
     * Add favorite to the DB.
     *
     * @param mealDetails
     */
    suspend fun addFavorite(mealDetails: MealDetails) = withContext(Dispatchers.IO) {
        val favorite = Favorite(
            mealDetails.id.toInt(),
            mealDetails.instructions,
            mealDetails.mealName,
            mealDetails.mealThumb
        )
        favDao.insertAll(favorite)
    }

    /**
     * Remove favorite from db.
     *
     * @param favorite
     */
    suspend fun removeFavorite(favorite: Favorite) = withContext(Dispatchers.IO) {
        favDao.delete(favorite)
    }

    /**
     * Get favorites from db.
     *
     * @return the [List] of [Favorite]
     */
    suspend fun getFavorites(): List<Favorite> = withContext(Dispatchers.IO) {
        return@withContext favDao.getAll()
    }

    companion object {
        enum class FilterType { CATEGORY, AREA, INGREDIENT }
    }
}
