package com.example.meals.model.dto.category

@kotlinx.serialization.Serializable
data class CategoryDTO(
    val idCategory: String,
    val strCategory: String,
    val strCategoryDescription: String,
    val strCategoryThumb: String
)
