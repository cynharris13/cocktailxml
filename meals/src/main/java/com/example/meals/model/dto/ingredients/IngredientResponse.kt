package com.example.meals.model.dto.ingredients

@kotlinx.serialization.Serializable
data class IngredientResponse(
    val meals: List<IngredientDTO> = emptyList()
)
