package com.example.meals.model.entity

/**
 * Meal details.
 *
 * @property id
 * @property instructions
 * @property mealName
 * @property mealThumb
 * @property ingredientsToMeasures
 * @constructor Create empty Meal details
 */
data class MealDetails(
    val id: String,
    val instructions: String,
    val mealName: String,
    val mealThumb: String,
    val ingredientsToMeasures: Map<String, String>
)
