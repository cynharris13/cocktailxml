package com.example.meals.model.remote

import com.example.meals.model.dto.area.AreaResponse
import com.example.meals.model.dto.category.CategoryResponse
import com.example.meals.model.dto.category.MealByFilterResponse
import com.example.meals.model.dto.ingredients.IngredientResponse
import com.example.meals.model.dto.mealdetails.MealDetailsResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Meal service to fetch from the api.
 *
 * @constructor Create empty Meal service
 */
interface MealService {
    @GET(CATEGORIES_ENDPOINT)
    suspend fun getMealCategories(): Response<CategoryResponse>

    @GET(LIST_ENDPOINT + AREA)
    suspend fun getAreas(): Response<AreaResponse>

    @GET(LIST_ENDPOINT + INGREDIENTS)
    suspend fun getIngredients(): Response<IngredientResponse>

    @GET(FILTER_ENDPOINT)
    suspend fun getMealByCategory(@Query("c") categoryName: String): Response<MealByFilterResponse>

    @GET(FILTER_ENDPOINT)
    suspend fun getMealByArea(@Query("a") areaName: String): Response<MealByFilterResponse>

    @GET(FILTER_ENDPOINT)
    suspend fun getMealByIngredient(@Query("i") ingredientName: String): Response<MealByFilterResponse>

    @GET(DETAILS)
    suspend fun getMealDetailsById(@Query("i") id: String): Response<MealDetailsResponse>

    companion object {
        const val CATEGORIES_ENDPOINT = "categories.php"
        const val LIST_ENDPOINT = "list.php"
        const val FILTER_ENDPOINT = "filter.php"
        const val AREA = "?a=list"
        const val INGREDIENTS = "?i=list"
        const val DETAILS = "lookup.php"
    }
}
