package com.example.meals.model.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Favorite(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "instructions") val instructions: String,
    @ColumnInfo(name = "meal_name") val mealName: String,
    @ColumnInfo(name = "meal_thumb") val mealThumb: String
)
