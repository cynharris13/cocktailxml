package com.example.meals.model.entity

/**
 * Ingredient data holder.
 *
 * @property ingredientName
 * @constructor Create empty Ingredient
 */
data class Ingredient(
    val ingredientName: String
)
