package com.example.meals.model.entity

/**
 * Meal by category.
 *
 * @property mealId
 * @property mealName
 * @property mealThumb
 * @constructor Create empty Meal by category
 */
data class MealByFilter(
    val mealId: String,
    val mealName: String,
    val mealThumb: String
)
