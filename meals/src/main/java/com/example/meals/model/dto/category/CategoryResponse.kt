package com.example.meals.model.dto.category

@kotlinx.serialization.Serializable
data class CategoryResponse(
    val categories: List<CategoryDTO> = emptyList()
)
