package com.example.meals.model.dto.area

@kotlinx.serialization.Serializable
data class AreaResponse(
    val meals: List<AreaDTO> = emptyList()
)
