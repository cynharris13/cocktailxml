package com.example.meals.model.dto.category

@kotlinx.serialization.Serializable
data class MealByFilterResponse(
    val meals: List<MealByFilterDTO> = emptyList()
)
