package com.example.meals.model.dto.area

@kotlinx.serialization.Serializable
data class AreaDTO(
    val strArea: String
)
