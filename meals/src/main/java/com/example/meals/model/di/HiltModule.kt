package com.example.meals.model.di

import androidx.room.Room
import com.example.meals.MealApp
import com.example.meals.model.local.FavoriteDatabase
import com.example.meals.model.remote.MealService
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Retrofit
import retrofit2.create

@OptIn(ExperimentalSerializationApi::class)
@Module
@InstallIn(SingletonComponent::class)
object HiltModule {
    private val json = Json {
        explicitNulls = false
        ignoreUnknownKeys = true
        coerceInputValues = true
    }

    @Provides
    fun providesRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://www.themealdb.com/api/json/v1/1/")
            .addConverterFactory(json.asConverterFactory("application/json".toMediaType()))
            .build()
    }

    @Provides
    fun providesMealService(retrofit: Retrofit): MealService = retrofit.create()

    @Provides
    fun providesFavoriteDB(): FavoriteDatabase {
        return Room.databaseBuilder(
            MealApp.appContext(),
            FavoriteDatabase::class.java,
            "favorite-database"
        ).build()
    }
}
