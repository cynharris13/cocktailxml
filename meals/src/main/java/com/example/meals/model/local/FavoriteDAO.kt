package com.example.meals.model.local

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.example.meals.model.entity.Favorite

@Dao
interface FavoriteDAO {
    @Query("SELECT * FROM favorite")
    fun getAll(): List<Favorite>

    @Insert(onConflict = REPLACE)
    fun insertAll(vararg favorites: Favorite)

    @Delete
    fun delete(favorite: Favorite)
}
