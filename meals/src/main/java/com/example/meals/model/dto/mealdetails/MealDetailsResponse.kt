package com.example.meals.model.dto.mealdetails

@kotlinx.serialization.Serializable
data class MealDetailsResponse(
    val meals: List<MealDetailsDTO> = emptyList()
)
