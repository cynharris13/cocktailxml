package com.example.meals.model.dto.ingredients

@kotlinx.serialization.Serializable
data class IngredientDTO(
    val idIngredient: String = "",
    val strDescription: String = "",
    val strIngredient: String = "",
    val strType: String = ""
)
