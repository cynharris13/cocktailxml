package com.example.meals.model.entity

/**
 * Category data object.
 *
 * @property categoryName
 * @property categoryDescription
 * @property categoryThumb image
 * @constructor Create empty Category
 */
data class Category(
    val categoryName: String,
    val categoryDescription: String,
    val categoryThumb: String
)
