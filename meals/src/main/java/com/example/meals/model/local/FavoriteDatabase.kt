package com.example.meals.model.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.meals.model.entity.Favorite

@Database(entities = [Favorite::class], version = 1)
abstract class FavoriteDatabase : RoomDatabase() {
    abstract fun favoriteDao(): FavoriteDAO
}
