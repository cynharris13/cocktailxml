package com.example.meals.model.dto.category

@kotlinx.serialization.Serializable
data class MealByFilterDTO(
    val idMeal: String,
    val strMeal: String,
    val strMealThumb: String
)
