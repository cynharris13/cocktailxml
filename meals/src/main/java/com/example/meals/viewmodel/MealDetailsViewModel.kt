package com.example.meals.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.meals.model.MealRepo
import com.example.meals.model.entity.Favorite
import com.example.meals.model.entity.MealDetails
import com.example.meals.view.mealdetails.MealState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Meal details view model.
 *
 * @property repo to make requests
 * @constructor Create empty Meal details view model
 */
@HiltViewModel
class MealDetailsViewModel @Inject constructor(private val repo: MealRepo) : ViewModel() {
    private val _state = MutableLiveData(MealState())
    val state: LiveData<MealState> get() = _state

    /**
     * Get meal details.
     *
     * @param id the id of the meal to fetch
     */
    fun getMealDetails(id: String) = viewModelScope.launch {
        _state.value = _state.value?.copy(isLoading = true)
        val meal = repo.getMealDetails(id)
        checkIsFavorite(meal)
        _state.value = _state.value?.copy(isLoading = false, meal = meal)
    }

    /**
     * Find if meal already exists as a favorite.
     *
     */
    private fun checkIsFavorite(meal: MealDetails) = viewModelScope.launch {
        _state.value = _state.value?.copy(isLoading = true)
        val mealList = repo.getFavorites()
        val isFavorite = mealList.find { it.id == meal.id.toInt() } != null
        _state.value = _state.value?.copy(isLoading = false, isFavorite = isFavorite)
    }

    /**
     * Add favorite.
     *
     * @param mealDetails
     */
    fun addFavorite(mealDetails: MealDetails) = viewModelScope.launch {
        repo.addFavorite(mealDetails)
    }

    /**
     * Remove favorite.
     *
     * @param mealDetails
     */
    fun removeFavorite(mealDetails: MealDetails) = viewModelScope.launch() {
        val favorite = Favorite(
            id = mealDetails.id.toInt(),
            instructions = mealDetails.instructions,
            mealName = mealDetails.mealName,
            mealThumb = mealDetails.mealThumb
        )
        repo.removeFavorite(favorite)
    }
}
