package com.example.meals.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.meals.model.MealRepo
import com.example.meals.model.entity.Favorite
import com.example.meals.view.saveditems.FavoriteState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Saved items view model.
 *
 * @property repo
 * @constructor Create empty Saved items view model
 */
@HiltViewModel
class SavedItemsViewModel @Inject constructor(private val repo: MealRepo) : ViewModel() {
    private val _state = MutableLiveData(FavoriteState())
    val state: LiveData<FavoriteState> get() = _state

    init {
        getFavorites()
    }

    private fun getFavorites() = viewModelScope.launch {
        _state.value = _state.value?.copy(isLoading = true)
        val favorites = repo.getFavorites()
        _state.value = _state.value?.copy(isLoading = false, favorites = favorites)
    }

    /**
     * Remove favorite.
     *
     * @param favorite
     */
    fun removeFavorite(favorite: Favorite) = viewModelScope.launch {
        _state.value = _state.value?.copy(isLoading = true)
        repo.removeFavorite(favorite)
        val favorites: MutableList<Favorite> = state.value?.favorites?.toMutableList() ?: mutableListOf()
        favorites.remove(favorite)
        _state.value = _state.value?.copy(isLoading = false, favorites = favorites)
    }
}
