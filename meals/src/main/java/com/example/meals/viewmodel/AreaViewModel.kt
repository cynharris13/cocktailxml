package com.example.meals.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.meals.model.MealRepo
import com.example.meals.model.entity.Area
import com.example.meals.view.area.AreaState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Area view model.
 *
 * @property repo to fetch
 * @constructor Create empty Area view model
 */
@HiltViewModel
class AreaViewModel @Inject constructor(private val repo: MealRepo) : ViewModel() {
    private val _state = MutableLiveData(AreaState())
    val state: LiveData<AreaState> get() = _state

    init {
        getAreas()
    }

    /**
     * Get areas from the [repo].
     *
     */
    private fun getAreas() = viewModelScope.launch {
        _state.value = _state.value?.copy(isLoading = true)
        val areas = repo.getAreas()
        _state.value = _state.value?.copy(isLoading = false, areas = areas)
    }

    /**
     * Get meal by area.
     *
     * @param area to fetch
     */
    fun getMealByArea(area: Area) = viewModelScope.launch {
        _state.value = _state.value?.copy(isLoading = true, selected = area)
        val mealsByArea = repo.getMealsByFilter(area.areaName, MealRepo.Companion.FilterType.AREA)
        _state.value = _state.value?.copy(isLoading = false, mealsByArea = mealsByArea)
    }
}
