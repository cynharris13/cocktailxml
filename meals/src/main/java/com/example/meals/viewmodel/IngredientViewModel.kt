package com.example.meals.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.meals.model.MealRepo
import com.example.meals.model.entity.Ingredient
import com.example.meals.view.ingredient.IngredientState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Ingredient view model.
 *
 * @property repo to fetch
 * @constructor Create empty ingredient view model
 */
@HiltViewModel
class IngredientViewModel @Inject constructor(private val repo: MealRepo) : ViewModel() {
    private val _state = MutableLiveData(IngredientState())
    val state: LiveData<IngredientState> get() = _state

    init {
        getIngredients()
    }

    /**
     * Get areas from the [repo].
     *
     */
    private fun getIngredients() = viewModelScope.launch {
        _state.value = _state.value?.copy(isLoading = true)
        val ingredients = repo.getIngredients()
        _state.value = _state.value?.copy(isLoading = false, ingredients = ingredients)
    }

    /**
     * Get meal by ingredient.
     *
     * @param ingredient to fetch
     */
    fun getMealByIngredient(ingredient: Ingredient) = viewModelScope.launch {
        _state.value = _state.value?.copy(isLoading = true, selected = ingredient)
        val mealsByIngredient = repo.getMealsByFilter(
            ingredient.ingredientName,
            MealRepo.Companion.FilterType.INGREDIENT
        )
        _state.value = _state.value?.copy(isLoading = false, mealsByIngredient = mealsByIngredient)
    }
}
