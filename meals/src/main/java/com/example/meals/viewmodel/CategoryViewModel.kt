package com.example.meals.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.meals.model.MealRepo
import com.example.meals.model.entity.Category
import com.example.meals.view.category.CategoryState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Category view model.
 *
 * @property repo to pull data from
 * @constructor Create empty Category view model
 */
@HiltViewModel
class CategoryViewModel @Inject constructor(private val repo: MealRepo) : ViewModel() {
    private val _state = MutableLiveData(CategoryState())
    val state: LiveData<CategoryState> get() = _state

    init {
        getCategories()
    }

    private fun getCategories() = viewModelScope.launch {
        _state.value = _state.value?.copy(isLoading = true)
        val categories = repo.getCategories()
        _state.value = _state.value?.copy(isLoading = false, categories = categories)
    }

    /**
     * Get meal by category.
     *
     * @param category to fetch
     */
    fun getMealByCategory(category: Category) = viewModelScope.launch {
        _state.value = _state.value?.copy(isLoading = true, selected = category)
        val mealsByCategory = repo.getMealsByFilter(category.categoryName, MealRepo.Companion.FilterType.CATEGORY)
        _state.value = _state.value?.copy(isLoading = false, mealsByCategory = mealsByCategory)
    }
}
