package com.example.meals.model

import com.example.meals.model.dto.area.AreaDTO
import com.example.meals.model.dto.area.AreaResponse
import com.example.meals.model.dto.category.CategoryDTO
import com.example.meals.model.dto.category.CategoryResponse
import com.example.meals.model.dto.category.MealByFilterDTO
import com.example.meals.model.dto.category.MealByFilterResponse
import com.example.meals.model.dto.ingredients.IngredientDTO
import com.example.meals.model.dto.ingredients.IngredientResponse
import com.example.meals.model.dto.mealdetails.MealDetailsDTO
import com.example.meals.model.dto.mealdetails.MealDetailsResponse
import com.example.meals.model.entity.Area
import com.example.meals.model.entity.Category
import com.example.meals.model.entity.Favorite
import com.example.meals.model.entity.Ingredient
import com.example.meals.model.entity.MealByFilter
import com.example.meals.model.entity.MealDetails
import com.example.meals.model.local.FavoriteDAO
import com.example.meals.model.local.FavoriteDatabase
import com.example.meals.model.remote.MealService
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

@OptIn(ExperimentalCoroutinesApi::class)
internal class MealRepoTest {
    private val service = mockk<MealService>()
    private val db = mockk<FavoriteDatabase>()
    private val repo by lazy { MealRepo(service, db) }
    private val dao = mockk<FavoriteDAO>()

    @BeforeEach
    fun setup() {
        coEvery { db.favoriteDao() } coAnswers { dao }
    }

    @Test
    fun testGetCategories() = runTest {
        // given
        val dtos = CategoryResponse(listOf(CategoryDTO("", "lol", "", "")))
        coEvery { service.getMealCategories().isSuccessful } coAnswers { true }
        coEvery { service.getMealCategories().body() } coAnswers { dtos }
        val expected = listOf(Category("lol", "", ""))

        // when
        val actual = repo.getCategories()

        // then
        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun testGetAreas() = runTest {
        // given
        val dtos = AreaResponse(listOf(AreaDTO("lol")))
        coEvery { service.getAreas().isSuccessful } coAnswers { true }
        coEvery { service.getAreas().body() } coAnswers { dtos }
        val expected = listOf(Area("lol"))

        // when
        val actual = repo.getAreas()

        // then
        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun testGetIngredients() = runTest {
        // given
        val dtos = IngredientResponse(listOf(IngredientDTO(strIngredient = "lol")))
        coEvery { service.getIngredients().isSuccessful } coAnswers { true }
        coEvery { service.getIngredients().body() } coAnswers { dtos }
        val expected = listOf(Ingredient("lol"))

        // when
        val actual = repo.getIngredients()

        // then
        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun testGetMealByFilter() = runTest {
        val dtos = MealByFilterResponse(listOf(MealByFilterDTO("", "lol", "")))
        coEvery { service.getMealByCategory("lol").isSuccessful } coAnswers { true }
        coEvery { service.getMealByCategory("lol").body() } coAnswers { dtos }
        val expected = listOf(MealByFilter("", "lol", ""))

        // when
        val actual = repo.getMealsByFilter("lol", MealRepo.Companion.FilterType.CATEGORY)

        // then
        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun testGetMealDetails() = runTest {
        val dtos = MealDetailsResponse(listOf(MealDetailsDTO(idMeal = "0", strMeal = "lol")))
        coEvery { service.getMealDetailsById("0").isSuccessful } coAnswers { true }
        coEvery { service.getMealDetailsById("0").body() } coAnswers { dtos }
        val expected = MealDetails("0", "", "lol", "", emptyMap())

        // when
        val actual = repo.getMealDetails("0")

        // then
        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun testGetFavorites() = runTest {
        // given
        val expected = listOf(Favorite(0, "", "lol", ""))
        coEvery { dao.getAll() } coAnswers { expected }

        // when
        val actual = repo.getFavorites()

        // then
        Assertions.assertEquals(expected, actual)
    }
}
