package com.example.meals.viewmodel

import com.example.meals.CoroutinesTestExtension
import com.example.meals.InstantTaskExecutorExtension
import com.example.meals.model.MealRepo
import com.example.meals.model.entity.Favorite
import com.example.meals.model.entity.MealDetails
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@OptIn(ExperimentalCoroutinesApi::class)
@ExtendWith(InstantTaskExecutorExtension::class)
@ExtendWith(CoroutinesTestExtension::class)
internal class MealDetailsViewModelTest {
    private val repo = mockk<MealRepo>()
    private val mealDetailsVM = MealDetailsViewModel(repo)

    @Test
    fun testGetMealDetails() = runTest {
        // given
        val expected = MealDetails("0", "", "food", "", emptyMap())
        coEvery { repo.getMealDetails("lol") } coAnswers { expected }
        val favorites = listOf(Favorite(0, "", "food", ""))
        coEvery { repo.getFavorites() } coAnswers { favorites }

        // when
        mealDetailsVM.getMealDetails("lol")

        // then
        Assertions.assertFalse(mealDetailsVM.state.value?.isLoading ?: true)
        Assertions.assertEquals(expected, mealDetailsVM.state.value?.meal)
        Assertions.assertTrue(mealDetailsVM.state.value?.isFavorite ?: false)
    }
}
