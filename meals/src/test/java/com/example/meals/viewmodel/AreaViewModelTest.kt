package com.example.meals.viewmodel

import com.example.meals.CoroutinesTestExtension
import com.example.meals.InstantTaskExecutorExtension
import com.example.meals.model.MealRepo
import com.example.meals.model.entity.Area
import com.example.meals.model.entity.MealByFilter
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@OptIn(ExperimentalCoroutinesApi::class)
@ExtendWith(InstantTaskExecutorExtension::class)
@ExtendWith(CoroutinesTestExtension::class)
internal class AreaViewModelTest {
    private val repo = mockk<MealRepo>()

    @Test
    fun testGetAreas() = runTest {
        // given
        val expected = listOf(Area("lol"))
        coEvery { repo.getAreas() } coAnswers { expected }

        // when
        val areaVM = AreaViewModel(repo)

        // then
        Assertions.assertFalse(areaVM.state.value?.isLoading ?: true)
        Assertions.assertEquals(expected, areaVM.state.value?.areas)
    }

    @Test
    fun testGetMealByArea() = runTest {
        // given
        val areas = listOf(Area("lol"))
        coEvery { repo.getAreas() } coAnswers { areas }
        val areaVM = AreaViewModel(repo)
        val expected = listOf(MealByFilter("", "ye", ""))
        coEvery { repo.getMealsByFilter("lol", MealRepo.Companion.FilterType.AREA) } coAnswers { expected }

        // when
        areaVM.getMealByArea(areas[0])

        // then
        Assertions.assertFalse(areaVM.state.value?.isLoading ?: true)
        Assertions.assertEquals(expected, areaVM.state.value?.mealsByArea)
    }
}
