package com.example.meals.viewmodel

import com.example.meals.CoroutinesTestExtension
import com.example.meals.InstantTaskExecutorExtension
import com.example.meals.model.MealRepo
import com.example.meals.model.entity.Favorite
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@OptIn(ExperimentalCoroutinesApi::class)
@ExtendWith(InstantTaskExecutorExtension::class)
@ExtendWith(CoroutinesTestExtension::class)
internal class SavedItemsViewModelTest {
    private val repo = mockk<MealRepo>()

    @Test
    fun testGetFavorite() = runTest {
        // given
        val expected = listOf(Favorite(0, "", "", ""))
        coEvery { repo.getFavorites() } coAnswers { expected }

        // when
        val savedItemsViewModel = SavedItemsViewModel(repo)

        // then
        Assertions.assertFalse(savedItemsViewModel.state.value?.isLoading ?: true)
        Assertions.assertEquals(expected, savedItemsViewModel.state.value?.favorites)
    }

    @Test
    fun testRemoveFavorite() = runTest {
        // given
        val favorites = listOf(Favorite(0, "", "", ""))
        coEvery { repo.getFavorites() } coAnswers { favorites }
        val savedItemsViewModel = SavedItemsViewModel(repo)
        coEvery { repo.removeFavorite(favorites[0]) } coAnswers { }

        // when
        savedItemsViewModel.removeFavorite(favorites[0])

        // then
        Assertions.assertFalse(savedItemsViewModel.state.value?.isLoading ?: true)
        Assertions.assertEquals(emptyList<Favorite>(), savedItemsViewModel.state.value?.favorites)
    }
}
