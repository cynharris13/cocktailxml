package com.example.meals.viewmodel

import com.example.meals.CoroutinesTestExtension
import com.example.meals.InstantTaskExecutorExtension
import com.example.meals.model.MealRepo
import com.example.meals.model.entity.Category
import com.example.meals.model.entity.MealByFilter
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@OptIn(ExperimentalCoroutinesApi::class)
@ExtendWith(CoroutinesTestExtension::class)
@ExtendWith(InstantTaskExecutorExtension::class)
internal class CategoryViewModelTest {
    private val repo = mockk<MealRepo>()

    @Test
    fun testGetCategories() = runTest {
        // given
        val expected = listOf(Category("lol", "", ""))
        coEvery { repo.getCategories() } coAnswers { expected }

        // when
        val categoryVM = CategoryViewModel(repo)

        // then
        Assertions.assertFalse(categoryVM.state.value?.isLoading ?: true)
        Assertions.assertEquals(expected, categoryVM.state.value?.categories)
    }

    @Test
    fun testGetMealByCategory() = runTest {
        // given
        val categories = listOf(Category("lol", "", ""))
        coEvery { repo.getCategories() } coAnswers { categories }
        val categoryVM = CategoryViewModel(repo)
        val expected = listOf(MealByFilter("", "ye", ""))
        coEvery { repo.getMealsByFilter("lol", MealRepo.Companion.FilterType.CATEGORY) } coAnswers { expected }

        // when
        categoryVM.getMealByCategory(categories[0])

        // then
        Assertions.assertFalse(categoryVM.state.value?.isLoading ?: true)
        Assertions.assertEquals(expected, categoryVM.state.value?.mealsByCategory)
    }
}
