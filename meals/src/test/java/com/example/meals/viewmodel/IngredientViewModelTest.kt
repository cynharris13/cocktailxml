package com.example.meals.viewmodel

import com.example.meals.CoroutinesTestExtension
import com.example.meals.InstantTaskExecutorExtension
import com.example.meals.model.MealRepo
import com.example.meals.model.entity.Ingredient
import com.example.meals.model.entity.MealByFilter
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@OptIn(ExperimentalCoroutinesApi::class)
@ExtendWith(InstantTaskExecutorExtension::class)
@ExtendWith(CoroutinesTestExtension::class)
internal class IngredientViewModelTest {
    private val repo = mockk<MealRepo>()

    @Test
    fun testGetIngredients() = runTest {
        // given
        val expected = listOf(Ingredient("lol"))
        coEvery { repo.getIngredients() } coAnswers { expected }

        // when
        val ingredientVM = IngredientViewModel(repo)

        // then
        Assertions.assertFalse(ingredientVM.state.value?.isLoading ?: true)
        Assertions.assertEquals(expected, ingredientVM.state.value?.ingredients)
    }

    @Test
    fun testGetMealByIngredient() = runTest {
        // given
        val ingredients = listOf(Ingredient("lol"))
        coEvery { repo.getIngredients() } coAnswers { ingredients }
        val ingredientVM = IngredientViewModel(repo)
        val expected = listOf(MealByFilter("", "ye", ""))
        coEvery { repo.getMealsByFilter("lol", MealRepo.Companion.FilterType.INGREDIENT) } coAnswers { expected }

        // when
        ingredientVM.getMealByIngredient(ingredients[0])

        // then
        Assertions.assertFalse(ingredientVM.state.value?.isLoading ?: true)
        Assertions.assertEquals(expected, ingredientVM.state.value?.mealsByIngredient)
    }
}
